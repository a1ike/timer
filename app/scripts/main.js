$(document).ready(function() {

  $('.phone').inputmask({
    mask: '+7(999)-999-99-99',
    showMaskOnHover: false
  });

  new Swiper('.t-home-carousel', {
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    loop: true,
    autoplay: {
      delay: 3000,
    },
    fadeEffect: {
      crossFade: true
    },
    effect: 'fade',
    speed: 750
  });

  new Swiper('.t-gallery__cards', {
    loop: true,
    autoplay: {
      delay: 5000,
    },
    spaceBetween: 50,
    slidesPerView: 'auto',
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    }
  });

  $('.open-modal').on('click', function(e) {
    e.preventDefault();
    $('.t-modal').toggle();
  });

  $('.t-modal__close').on('click', function(e) {
    e.preventDefault();
    $('.t-modal').toggle();
  });

  $('.t-header__toggle').on('click', function(e) {
    e.preventDefault();

    $('.t-top').slideToggle('fast');
    $('.t-header__main').slideToggle('fast');
    $('.t-header__right').slideToggle('fast');
  });

  $('.t-filter__title_collapse').on('click', function(e) {
    e.preventDefault();

    $(this).toggleClass('t-filter__title_collapse_active');
    $(this).next().slideToggle('fast');
  });

  $('.t-tabs__header li').click(function() {
    var tab_id = $(this).attr('data-tab');

    $('.t-tabs__header li').removeClass('current');
    $('.t-tabs__content').removeClass('current');

    $(this).addClass('current');
    $('#' + tab_id).addClass('current');
  });

  var galleryThumbs = new Swiper('.t-item__small', {
    spaceBetween: 1,
    slidesPerView: 'auto',
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
  });
  var galleryTop = new Swiper('.t-item__big', {
    spaceBetween: 10,
    thumbs: {
      swiper: galleryThumbs
    }
  });
});